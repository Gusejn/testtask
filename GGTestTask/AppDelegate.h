//
//  AppDelegate.h
//  GGTestTask
//
//  Created by Гусейн on 18/09/16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

