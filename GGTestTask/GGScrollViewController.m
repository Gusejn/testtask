//
//  GGScrollViewController.m
//  GGTestTask
//
//  Created by Гусейн on 18/09/16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import "GGScrollViewController.h"

@interface GGScrollViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *imageScrollView;
@property (assign, nonatomic) CGFloat yPosition;
@property (assign, nonatomic) CGFloat scrollContentSize;

@end

@implementation GGScrollViewController

#define queue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadImages];
}

- (void)loadImages {
    dispatch_group_t group = dispatch_group_create();
    for (int i = 0; i < 30; i++) {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://lorempixel.com/400/200"]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        dispatch_async(queue, ^{
            dispatch_group_enter(group);
            NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (data) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self setImageToScrollView:data];
                        dispatch_group_leave(group);
                    });
                }
            }];
            [downloadTask resume];
        });
    }
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [self showAlertController];
        
    });}

- (void)showAlertController {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Everything has completed"
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *doneAction = [UIAlertAction
                                 actionWithTitle:@"Done"
                                 style:UIAlertActionStyleDefault
                                 handler:nil];
    [alertController addAction:doneAction];
    [self presentViewController:alertController
                       animated:YES
                     completion:nil];
}

- (void)setImageToScrollView:(NSData *)data {
    UIImage *image = [UIImage imageWithData:data];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    CGRect frame = imageView.frame;
    frame.origin.y = _yPosition;
    imageView.frame = frame;
    [self.imageScrollView addSubview:imageView];
    self.yPosition+=imageView.frame.size.height;
    self.scrollContentSize+=imageView.frame.size.height;
    [self.imageScrollView setContentSize:CGSizeMake(imageView.frame.size.width, self.scrollContentSize)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
