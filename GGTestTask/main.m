//
//  main.m
//  GGTestTask
//
//  Created by Гусейн on 18/09/16.
//  Copyright © 2016 Гусейн. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
